# final_project

Финальный проект по блоку Deploy

![hell yeah](https://media.giphy.com/media/v8CtklH7wwSZ2/giphy.gif)


## Схема

[Схема реализована в Draw.io](./final_project_schema.drawio)

Постранично:
- Темплейты - изначальная страница с набором шаблонов
- Архитектура - архитектура сервиса с описанием компонентов и схемы обработки запросов
- Архитектура и конфигурация кластера индексов + доставка файлов индекса
- Процесс обновления системы - пошаговая схема обновления с пояснениями и примечаниями
- Разбивка на репозитории - схема разбивки с описанием пайплайнов и варианта реализации data generation(в самом низу)


## Контакты
https://t.me/Come_on00 - телеграм

